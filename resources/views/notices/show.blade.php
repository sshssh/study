<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공지사항 확인</title>
</head>
<style>
        table.table2{
                text-align: left;
                line-height: 1.5;
                border-top: 1px solid #ccc;
        }
        table.table2 tr {
                font-weight: bold;
                vertical-align: top;
                border-bottom: 1px solid #ccc;
        }
        table.table2 td {
                 width: 100px;
                 padding: 10px;
                 vertical-align: top;
                 border-bottom: 1px solid #ccc;
        }
 
</style>
<body>
<form method = "get" action = "/notices/{{ $notice->id }}/edit">
@csrf
        <table  style="padding-top:50px" align = center width=700 border=0 cellpadding=2 >
                <tr>
                <td height=40 align=center bgcolor=black><font color=white><b>공지사항 확인</font></b></td>
                </tr>

                <tr>
                <td bgcolor=white>
                <table class = "table2">
                    <tr>
                    <td>작성자</td>
                    <td><input type = text name = name size=30 readonly=readonly value="{{ $notice -> name }}"></td>
                    </tr>

                    <tr>
                    <td>제목</td>
                    <td><input type = text name = title size=60  readonly=readonly value="{{ $notice -> title }}"></td>
                    </tr>

                    <tr>
                    <td>내용</td>
                    <td><textarea name = contents cols=85  readonly=readonly rows=15>{{ $notice -> contents }}</textarea></td>
                    </tr>
                </table>

                <div align=right>
                <input type = "submit" value="수정">
                </div>
                
                </td>
                </tr>
        </table>
    </form>
    
</body>
</html>