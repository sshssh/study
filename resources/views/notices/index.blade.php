<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
    <title>메인 페이지</title>
</head>
<body>
        <div class="container">
        <h1>Notice List</h1>
                <table class="table table-striped">
                        <thead>
                                <tr>
                                <th>NO</th>
                                <th>제목</th>
                                <th>작성자</th>
                                <th>최종 수정 일자</th>
                                </tr>
                        </thead>
                        
                        <tbody>
                        
                        @foreach ($notices as $notice)
                                <tr>
                                        <td width="150"> <a href="/notices/{{$notice->id}}/show">{{ $notice -> id }} </a></td>
                                        <td width="220"> <a href="/notices/{{$notice->id}}/show">{{ $notice -> title }} </a></td>
                                        <td width="200"> <a href="/notices/{{$notice->id}}/show">{{ $notice -> name }} </a></td>
                                        <td width="200"> <a href="/notices/{{$notice->id}}/show">{{ $notice -> updated_at }} </a></td>
                                </tr>
                        @endforeach
                        </tbody>
                </table>
                <div>
                <a class="btn btn-default" href='/notices/create'>글쓰기</a>
                </div>
        </div>
</body>
</html>





































<!-- <thead>
                <tr>
                        <th width="200">number</th>
                        <th width="350">title</th>
                        <th width="250">name</th>
                        <th width="200">date</th>
                </tr>
                </thead>
                <tr>
                        <td width="200">d</td>
                        <td width="350">d</td>
                        <td width="250">d</td>
                        <td width="200">d</td>
                </tr> -->