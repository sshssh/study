<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notice;
use DB;

class NoticeController extends Controller
{

    public function index()
    {
        $notices = Notice::all();
        return view('notices.index', [
            'notices' => $notices
            ]);
    }



    public function create()
    {
        return view('notices.create');
    }


    
    public function store(Request $request)
    {
      $notice = Notice::create([ 
          'name'=> $request->input('name'),
          'title' => $request->input('title'),
          'contents'=> $request->input('contents')
      ]);
      
        return redirect('/notices');
    }
   


    public function show(Notice $notice)
    {
        return view('notices.show',[
            'notice' => $notice
            ]);
    }



    public function edit(Notice $notice)
    {   
        return view('notices.edit',[
            'notice' => $notice
            ]);
    }


    public function update(Request $request)
    {
        
        $notice_id = $request->input('id');
        $notice= Notice::find($notice_id);
        $notice->name= $request->input('name');
        // $notice->save();
        
        return redirect('/notices');
    }


}