<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/notices', 'NoticeController@index');

Route::get('/notices/create', 'NoticeController@create');

Route::post('/notices/store', 'NoticeController@store');

Route::get('/notices/{notice}/show', 'NoticeController@show');

Route::get('/notices/{notice}/edit', 'NoticeController@edit');

Route::put('/notices/{notice}', 'NoticeController@update');
// Route::resource('notice', 'NoticeController');